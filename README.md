# Info

To compile the project using javac from the command line

```shell
javac -d ./out -cp src/ src/ro/aduzsardi/Main.java
```

Package it in a java archive

```shell
jar -cvmf manifest.mf TitleCaseConverter.jar -C out/ .
```

Run the app

```shell
java -cp out/ ro.aduzsardi.Main
# or if packaged
java -jar TitleCaseConverter.jar
```
