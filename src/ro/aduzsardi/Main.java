package ro.aduzsardi;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.plaf.DimensionUIResource;

public class Main {
    public static void main(String... args) {
        JFrame application = createGUI();
        application.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        application.setVisible(true);
    }

    private static JFrame createGUI() {
        JTextField input = new JTextField();
        input.setPreferredSize(new DimensionUIResource(300,40));


        JButton converterButton = new JButton("Convert");
        JLabel output = new JLabel();
        output.setPreferredSize(new DimensionUIResource(300,40));

        converterButton.addActionListener(event -> {
            output.setText(TitleCaseConverter.convertToTitleCase(input.getText()));
        });

        JFrame gui = new JFrame("Title case converter");
        gui.setLayout(new FlowLayout());
        gui.add(input);
        gui.add(converterButton);
        gui.add(output);
        gui.pack();
        gui.setLocationRelativeTo(null);

            
        return gui;
    }
}
